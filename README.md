# samlachance.com

A professional web site written in Crystal.

## System requirements

 - Crystal `= 0.26.0` with Shards

## Getting started

```bash
# Install dependencies
$ shards install

# Compile application
$ shards build

# Run application
$ bin/app
```
