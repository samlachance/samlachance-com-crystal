require "kemal"

# Disable "X-Powered-By: Kemal" header
Kemal.config.powered_by_header = false

# Homepage
get "/" do |env|
  env.params.query.each do |key, value|
    puts "#{key}: #{value}"
  end
  render "src/views/index.ecr"
end

# Exception page for demonstration purposes
get "/exception" do
  raise Exception.new("Derp")
end

Kemal.run
